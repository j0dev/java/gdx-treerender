package nl.j0dev.gdx.treerender.g3d;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Container is the basis for the visual component system,
 * Consisting of position and group based system
 */
public class Container implements ContainerNode {

    // transformation storage
    protected final Transformation transformation = new Transformation();

    // group / nesting system storage
    protected ContainerNode parent = null;
    protected final List<ContainerNode> children = new ArrayList<>();


    /**
     * Get the transformation form the visual component
     */
    public Transformation getTransformation() {
        return transformation;
    }


    @Override
    public boolean isTopNode() {
        return this.parent == null;
    }
    @Override
    public boolean hasParent() {
        return this.parent != null;
    }
    @Override
    public ContainerNode getParent() {
        return this.parent;
    }
    @Override
    public void setParent(ContainerNode parent) {
        this.parent = parent;
        if (parent != null) {
            this.transformation.setParent(parent.getTransformation());
        }
    }

    @Override
    public boolean hasChildren() {
        return this.children.size() > 0;
    }
    @Override
    public Collection<? extends ContainerNode> getChildren() {
        return List.copyOf(this.children);
    }
    @Override
    public void addChild(ContainerNode child) {
        this.children.add(child);
        // update the child's parent
        child.setParent(this);
    }
    @Override
    public void addChildren(Collection<? extends ContainerNode> children) {
        this.children.addAll(children);
        // update the children's parent
        children.forEach(visComponent -> visComponent.setParent(this));
    }
    @Override
    public void removeChild(ContainerNode child) {
        boolean remove = this.children.remove(child);
        if (remove) {
            // update the child's parent
            child.setParent(null);
        }
    }
    @Override
    public void clearChildren() {
        // update the children's parent
        this.children.forEach(visComponent -> visComponent.setParent(null));
        this.children.clear();
    }


    // This implementation also includes the rendering call to renderer-able nodes (that inherit from this container)
    @Override
    public void doRender(ModelBatch batch, Environment environment) {
        // render ourselves if we are a render-able component
        if (this instanceof RenderableNode) {
            ((RenderableNode) this).render(batch, environment);
        }

        // render children
        for (ContainerNode child : this.children) {
            child.doRender(batch, environment);
        }
    }
}
