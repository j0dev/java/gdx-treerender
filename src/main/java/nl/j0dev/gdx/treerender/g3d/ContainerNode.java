package nl.j0dev.gdx.treerender.g3d;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

import java.util.Collection;

/**
 * ContainerNode is the top level interface to the rendering system.
 * ContainerNode represents a node in the tree able to hold child nodes and proxy their rendering. (may be the top node)
 */
public interface ContainerNode {

    /**
     * Returns the transformation of the node consisting both relative and absolute position in the tree
     * @return transformation
     */
    Transformation getTransformation();

    /**
     * Returns if the node is at the top
     * @return is top node
     */
    boolean isTopNode();
    /**
     * Returns if the node has a parent
     * @return has parent
     */
    boolean hasParent();
    /**
     * Returns the parent node if set or null if it is the top node
     * @return Parent or null
     */
    ContainerNode getParent();
    /**
     * Sets the parent of the node, replacing any previous values
     * @param parent new parent
     */
    void setParent(ContainerNode parent);

    /**
     * Returns if the node has any children
     * @return has children
     */
    boolean hasChildren();
    /**
     * Returns a read-only collection of children
     * @return read-only collection of children
     */
    Collection<? extends ContainerNode> getChildren();
    /**
     * Add a child to the node
     * @param child child
     */
    void addChild(ContainerNode child);
    /**
     * Add a collection of children to the node
     * @param children collection of children
     */
    void addChildren(Collection<? extends ContainerNode> children);
    /**
     * Removes a specific child if present
     * @param child child node
     */
    void removeChild(ContainerNode child);
    /**
     * Removes all children from the node
     */
    void clearChildren();

    /**
     * Method to proxy rendering.
     * Triggers rendering on the node and all children
     * @param batch Batch to render on
     * @param environment Environment to render in
     */
    void doRender(ModelBatch batch, Environment environment);
}
