package nl.j0dev.gdx.treerender.g3d;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;

/**
 * RenderableModel is an abstract implementation of a model node that can render itself.
 * Given a few simple properties it will render and update itself without having to write any core
 */
public abstract class RenderableModel extends Container implements RenderableNode {
    /**
     * Returns the 3D model file to load and render
     * @return 3D model asset file
     */
    public abstract String getAsset();

    protected final Model model;
    protected ModelInstance modelInstance;

    /**
     * Instantiate the model
     * @param assets asset manager
     */
    protected RenderableModel(AssetManager assets) {
        if (!assets.contains(this.getAsset())) {
            assets.load(this.getAsset(), Model.class);
            // block until loaded
            assets.finishLoadingAsset(this.getAsset());
        }
        this.model = assets.get(this.getAsset(), Model.class);

        // automatically create the object
        this.create();
    }

    /**
     * Re-create the model after disposing it
     */
    public void create() {
        this.modelInstance = new ModelInstance(this.model);
        this.modelInstance.transform.set(this.transformation.getWorldTransformationMatrix());
    }

    @Override
    public void render(ModelBatch batch, Environment environment) {
        this.update();
        this.modelInstance.transform.set(this.transformation.getWorldTransformationMatrix());
        batch.render(this.modelInstance, environment);
    }

    /**
     * Method that gets called every frame that gets rendered
     * If you need to do anything special during the rendering, this is where it should go
     */
    public void update() {
    }

    /**
     * Properly dispose of the model
     */
    public void dispose() {
        this.modelInstance = null;
    }
}
