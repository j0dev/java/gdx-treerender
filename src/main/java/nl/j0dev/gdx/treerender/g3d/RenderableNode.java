package nl.j0dev.gdx.treerender.g3d;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

/**
 * RenderableNode is the interface for render-able nodes in the rendering system
 * RenderableNode also inherits from ContainerNode to allow render-able nodes to also contain children and handle the transformations.
 */
public interface RenderableNode extends ContainerNode {

    /**
     * Render call to render the node
     * @param batch Batch to render on
     * @param environment Environment to render in
     */
    void render(ModelBatch batch, Environment environment);
}
