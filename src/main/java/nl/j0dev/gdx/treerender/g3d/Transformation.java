package nl.j0dev.gdx.treerender.g3d;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

/**
 * Transformation holds the positional references of an object,
 * and allows for relative positioning of the object in relation to a parent
 */
public class Transformation {
    /**
     * Position of the object (relative to its parent)
     */
    protected Vector3 position = new Vector3();

    /**
     * Orientation of the object (relative to its parent)
     *
     * Normally one would be using a Vector3 (pitch, yaw, roll)
     * We are currently using just a float as we currently only need one orientation direction
     */
    protected Quaternion orientation = new Quaternion();

    /**
     * Scale of the object (relative to its parent)
     */
    protected Vector3 scale = new Vector3(1, 1, 1);

    /**
     * Parent of the object, or null
     */
    protected Transformation parent;


    /**
     * Get the relative position of the object in relation to its parent
     * @return relative position
     */
    public Vector3 getPosition() {
        // return a copy, so changes don't propagate back, requiring a set() and allows for use in calculations without explicit copying
        return this.position.cpy();
    }
    /**
     * Set the relative position of the object in relation to its parent
     * @param position new relative position
     */
    public void setPosition(Vector3 position) {
        this.position = position.cpy();
    }

    /**
     * Get the absolute world position of the object
     * @return world position
     */
    public Vector3 getWorldPosition() {
        // if there is no parent, we are the top most object
        if (this.parent == null) return this.getPosition();

        // get the transformation
        return this.getWorldTransformationMatrix().getTranslation(new Vector3());
    }
    /**
     * Set the absolute world position of the object
     * internally will translate the world position to a local position relative to its parent
     * @param position world position
     */
    public void setWorldPosition(Vector3 position) {
        if (this.parent == null) this.setPosition(position);

        // TODO: translate world position to relative from parent
    }


    /**
     * Get the local orientation of the object in relation to its parent
     * @return relative orientation
     */
    public Quaternion getOrientation() {
        return this.orientation.cpy();
    }
    /**
     * Set the relative orientation of the object in relation to its parent
     * @param orientation relative orientation
     */
    public void setOrientation(Quaternion orientation) {
        this.orientation = orientation.cpy();
    }

    /**
     * Get the absolute world orientation of the object
     * @return world orientation
     */
    public Quaternion getWorldOrientation() {
        if (this.parent == null) return this.getOrientation();

        // TODO: figure out how to calculate with the quaternion
        return new Quaternion();
    }
    /**
     * Set the absolute world orientation of the object
     * @param orientation world orientation
     */
    public void setWorldOrientation(Quaternion orientation) {
        if (this.parent == null) this.setOrientation(orientation);

        // TODO translate world orientation to relative from parent
    }


    /**
     * Get the local scale of the object in relation to its parent
     * @return relative scale
     */
    public Vector3 getScale() {
        return this.scale.cpy();
    }

    /**
     * Set the local scale of the object in relation to its parent
     * @param scale local scale
     */
    public void setScale(Vector3 scale) {
        this.scale = scale.cpy();
    }

    /**
     * Set the local scale of the object in relation to its parent
     * @param scale local scale
     */
    public void setScale(int scale) {
        this.scale.set(scale, scale, scale);
    }
    /**
     * Set the local scale of the object in relation to its parent
     * @param scale local scale
     */
    public void setScale(double scale) {
        this.scale.set((float) scale, (float) scale, (float) scale);
    }
    /**
     * Set the local scale of the object in relation to its parent
     * @param scale local scale
     */
    public void setScale(float scale) {
        this.scale.set(scale, scale, scale);
    }

    // TODO world scale methods


    /**
     * Get the current parent of the object
     */
    public Transformation getParent() {
        return this.parent;
    }
    /**
     * Set / change the parent of the object.
     * This will move the object in the world, keeping its relative position according to its current parent
     */
    public void setParent(Transformation parent) {
        this.parent = parent;
    }


    /**
     * Get the local transformation matrix according to its parent
     */
    public Matrix4 getLocalTransformationMatrix() {
        return new Matrix4(position, this.orientation, this.scale);
    }
    /**
     * Get the transformation matrix according to the world
     */
    public Matrix4 getWorldTransformationMatrix() {
        if (parent == null) return getLocalTransformationMatrix();
        return parent.getWorldTransformationMatrix().mul(this.getLocalTransformationMatrix());
    }
}
